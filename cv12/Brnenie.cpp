

#include "Brnenie.h"

Brnenie::Brnenie(int vaha, int odolnost) {
    m_vaha = vaha;
    m_odolnost = odolnost;
}

void Brnenie::printInfo() {
    std::cout << "brnenie: " << std::endl;
    std::cout << " - vaha brnenia: " << m_vaha << std::endl;
    std::cout << " - bonus utoku brnenia: " << getBonusUtoku() << std::endl;
    std::cout << " - bonus obrany brnenia: " << getBonusObrany() << std::endl;
}