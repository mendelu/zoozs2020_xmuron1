

#ifndef CV_11_KRUZKOVEBRNENIE_H
#define CV_11_KRUZKOVEBRNENIE_H

#include "Brnenie.h"

class KruzkoveBrnenie : public Brnenie {
private:
    int m_ohybnost;
public:
    KruzkoveBrnenie(int vaha, int odolnost, int ohybnost);

    int getBonusUtoku() override;

    int getBonusObrany() override;

    void printInfo() override;
};


#endif //CV_11_KRUZKOVEBRNENIE_H
