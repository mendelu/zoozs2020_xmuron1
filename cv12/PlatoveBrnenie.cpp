

#include "PlatoveBrnenie.h"

PlatoveBrnenie::PlatoveBrnenie(int vaha,
                               int odolnost) : Brnenie(vaha, odolnost) {}

int PlatoveBrnenie::getBonusUtoku() {
    return m_odolnost / 2;
}

int PlatoveBrnenie::getBonusObrany() {
    return m_odolnost;
}

void PlatoveBrnenie::printInfo() {
    Brnenie::printInfo();
    std::cout << " - typ: platove brnenie" << std::endl;
    std::cout << " - odolnost: " << m_odolnost << std::endl;
}