

#include "Rytier.h"

Rytier::Rytier(std::string meno, int sila) {
    m_meno = meno;
    m_sila = sila;
    m_helma = nullptr;
    m_brnenie = nullptr;
}

void Rytier::setBrnenie(Brnenie *brnenie) {
    m_brnenie = brnenie;
}

void Rytier::setHelma(Helma *helma) {
    m_helma = helma;
}

void Rytier::printInfo() {
    std::cout << std::endl;
    std::cout << "------Rytier-------" << std::endl;
    std::cout << "meno: " << m_meno << std::endl;
    std::cout << "sila: " << m_sila << std::endl;
    std::cout << "total utok: " << getUtok() << std::endl;
    std::cout << "total obrana: " << getObrana() << std::endl;
    if (m_helma != nullptr) {
        m_helma->printInfo();
    }
    if (m_brnenie != nullptr) {
        m_brnenie->printInfo();
    }
}

int Rytier::getUtok() {
    int utok = m_sila;
    if (m_brnenie != nullptr) {
        utok += m_brnenie->getBonusUtoku();
    }
    return utok;
}

int Rytier::getObrana() {
    int obrana = m_sila;
    if (m_brnenie != nullptr) {
        obrana += m_brnenie->getBonusObrany();
    }
    if (m_helma != nullptr) {
        obrana += m_helma->getBonusObrany();
    }
    return obrana;
}
