#include <iostream>

using namespace std;


class Passenger {
    string m_name;
    float m_weight;
    float m_luggageWeight;

public:
    Passenger(string name, float weight, float luggageWeight) {
        m_name = name;
        m_weight = weight;
        m_luggageWeight = luggageWeight;
    }

    Passenger(string name, float luggageWeight) {
        m_name = name;
        m_luggageWeight = luggageWeight;
        m_weight = 70.0;
    }

    float getTotalWeight() {
        return m_weight + m_luggageWeight;
    }

    string getName() {
        return m_name;
    }

};


class Elevator {
    float m_maxWeight;
    float m_currentWeight;
    int m_passengers;

public:
    Elevator() {
        m_maxWeight = 200.0;
        m_currentWeight = 0.0;
        m_passengers = 0;
    }

    void addPassenger(Passenger *newPassenger) {
        float newWeight = m_currentWeight + newPassenger->getTotalWeight();

        if (newWeight <= m_maxWeight) {
            // mozem pridat pasaziera
            m_currentWeight = newWeight;
            m_passengers += 1;
            // m_passengers = m_passengers + 1;
        } else {
            // nemozem pridat pasaziera
            cout << "Pasaziera " << newPassenger->getName()
                 << " nie je mozne uz pridat!!!" << endl;
        }
    }

    void printInfo() {
        cout << "Aktualna vaha: " << m_currentWeight << endl;
        cout << "Priemerna vaha: " << calculateAverage() << endl;
        cout << "Pocet pasazierov: " << m_passengers << endl;
    }

private:
    float calculateAverage() {
        return m_currentWeight / m_passengers;
    }
};


int main() {
    Elevator *vytah = new Elevator();
    Passenger *mikulas = new Passenger("Mikulas", 120, 30);
    Passenger *tomas = new Passenger("Tomas", 20, 0.0);

    vytah->addPassenger(mikulas);
    vytah->addPassenger(tomas);

    vytah->printInfo();

    delete mikulas;
    delete tomas;
    delete vytah;
    return 0;
}
