#include <iostream>

using namespace std;

class Lektvar{
private:
    string m_jmeno;
    int m_bonusKZivotum;
public:
    Lektvar(string jmeno, int bonus){
        m_jmeno = jmeno;
        m_bonusKZivotum = bonus;
    }

    int getBonus(){
        return m_bonusKZivotum;
    }
};

class Zbran{
private:
    string m_jmeno;
    int m_bonusKUtoku;

public:
    Zbran(string jmeno, int bonus){
        m_jmeno = jmeno;
        m_bonusKUtoku = bonus;
    }

    string getJmeno(){
        return m_jmeno;
    }

    int getBonus(){
        return m_bonusKUtoku;
    }

};


class Hrdina{
private:
    string m_jmeno;
    int m_zivoty;
    int m_sila;
    Zbran* m_zbran;

public:
    Hrdina(string jmeno, int sila){
        m_jmeno = jmeno;
        m_sila = sila;
        m_zivoty = 100;
        m_zbran = nullptr;
    }

    void seberZbran(Zbran* zbranCoJsemSebral){
        m_zbran = zbranCoJsemSebral;
    }

    void zahodZbran(){
        m_zbran = nullptr;
    }

    int getZivoty(){
        return m_zivoty;
    }

    int getSila(){
        // nemam zbran
        if(m_zbran == nullptr){
            return m_sila;
        }
            // mam zbran, pocitam silu i se zbrani
        else{
            return m_sila + m_zbran->getBonus();
        }
    }

    void uberZivot(int kolik){
        m_zivoty = m_zivoty - kolik;
        if(m_zivoty < 0){
            cout << "Hrdina " << m_jmeno << " je mrtev" << endl;
        }
    }

    void printInfo(){
        cout << "Jsem hrdina: " << m_jmeno << endl;
        cout << "Mám " << getZivoty() << " zivotu" << endl;
        cout << "Moje sila je " << getSila() << endl;
        if(m_zbran == nullptr){
            cout << "Hrdina nema zadnou zbran" << endl;
        }
        else{
            cout << "Hrdina ma zbran " << m_zbran->getJmeno() << " s bonusem " << m_zbran->getBonus() << endl;
        }

    }

    void zautoc(Hrdina* nepritel){
        if(nepritel->getZivoty() > 0) {
            int kolikUbrat = getSila();
            nepritel->uberZivot(kolikUbrat);
        }
        else{
            cout << "Utocis na mrtvolu!" << endl;
        }
    }

    void vypijLektvar(Lektvar* lektvarCoChcesVypit){
        m_zivoty = m_zivoty + lektvarCoChcesVypit->getBonus();
    }

};

int main() {
    Lektvar* pivo = new Lektvar("Pivo",50);
    Zbran* mec = new Zbran("Mec",5);
    Hrdina* artus = new Hrdina("Artus",100);
    artus->seberZbran(mec);
    artus->vypijLektvar(pivo);
    artus->printInfo();

    Hrdina* tom = new Hrdina("Tom",10);
    artus->zautoc(tom);
    artus->zautoc(tom);

    return 0;
}