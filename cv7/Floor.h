//
// Created by Tomas on 3. 11. 2020.
//

#ifndef CV7_FLOOR_H
#define CV7_FLOOR_H

#include <iostream>
#include <array>
#include "Container.h"

class Floor {
    std::string m_label;
    std::array<Container *, 10> m_containers;

public:
    Floor(std::string label);

    void storeContainer(int index, Container *newContainer);

    void removeContainer(int index);

    void printInfo();

    std::array<Container *, 10> getAllContainers();

    ~Floor();
};


#endif //CV7_FLOOR_H
