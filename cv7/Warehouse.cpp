//
// Created by Tomas on 3. 11. 2020.
//

#include "Warehouse.h"

Warehouse::Warehouse() {
    m_floors.push_back(new Floor("Floor n.0"));
}

void Warehouse::buildNewFloor() {
    m_floors.push_back(new Floor("Floor n." + std::to_string(m_floors.size())));
}

void Warehouse::destroyLastFloor() {
    if (!m_floors.empty()) {
        delete (m_floors.at(m_floors.size() - 1));
        m_floors.pop_back();
        std::cout << "Floor was destroyed!" << std::endl;
    } else {
        std::cout << "There is no more floor!" << std::endl;
    }
}

void Warehouse::storeContainer(int floor, int position, Container *container) {
    if (floor > m_floors.size() - 1) {
        std::cout << "Floor with index " << floor << " doesn't exist." << std::endl;
        return; // prerus tuto funkciu tu a skonci
    }

    // VOLITELNE: kontrola ci sa nahodou dany kontajner uz nenachadza v sklade (ci sa nevklada 2x)
    for (auto *f:m_floors) {
        for (auto *b:f->getAllContainers()) {
            if (b == container) {
                std::cout << "This container is already stored." << std::endl;
                return;
            }
        }
    }

    // uloz kontajner na danom poschodi na dane miesto
    m_floors.at(floor)->storeContainer(position, container);
}

void Warehouse::removeContainer(int floor, int position) {
    // odstran kontajner na danom poschodi z daneho miesta
    m_floors.at(floor)->removeContainer(position);
}

void Warehouse::printInfo() {
    std::cout << "Store info: " << std::endl;
    // druhy zapis for cyklu
    for (int i = 0; i < m_floors.size(); i++) {
        m_floors[i]->printInfo();
    }
}

Warehouse::~Warehouse() {
    for (auto *floor: m_floors) {
        delete (floor);
    }
}