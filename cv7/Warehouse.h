//
// Created by Tomas on 3. 11. 2020.
//

#ifndef CV7_WAREHOUSE_H
#define CV7_WAREHOUSE_H

#include <iostream>
#include <vector>
#include "Floor.h"

class Warehouse {
    std::vector<Floor *> m_floors;
public:
    Warehouse();

    void buildNewFloor();

    void destroyLastFloor();

    void storeContainer(int floor, int position, Container *container);

    void removeContainer(int floor, int position);

    void printInfo();

    ~Warehouse();
};


#endif //CV7_WAREHOUSE_H
