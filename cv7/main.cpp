#include <iostream>

#include "Container.h"
#include "Warehouse.h"

int main() {
    auto *container1 = new Container(10, "David");
    auto *container2 = new Container(200, "Mikulas");
    auto *container3 = new Container(300, "Tomas");
    auto *warehouse = new Warehouse();

    // postavit 2 poschodia
    warehouse->buildNewFloor();
    warehouse->buildNewFloor();

    // ulozit kontajnere
    warehouse->storeContainer(0, 5, container1);
    warehouse->storeContainer(1, 0, container2);

    // pokus ulozit ten isty kontajner 2x
    warehouse->storeContainer(2, 10, container2);

    warehouse->printInfo();

    // odstranit posledne poschodie
    warehouse->destroyLastFloor();

    warehouse->printInfo();

    delete container1;
    delete container2;
    delete container3;
    delete warehouse;
    return 0;
}
