#include <iostream>
#include "Automobil.h"
#include "Autosalon.h"


int main() {
    Automobil* automobil1 = new Automobil("BMW", 500000);
    Automobil* automobil2 = new Automobil("Skoda", 500000, 80, 25000);

    Autosalon* autosalon = new Autosalon();
    autosalon->pridejAutomobil(automobil1);
    autosalon->pridejAutomobil(automobil2);
    autosalon->pridejAutomobil("Volvo",5000000);

    autosalon->vypisAuta();

    std::cout << autosalon->getCelkovaCena() << "Kc" << std::endl;


    return 0;
}