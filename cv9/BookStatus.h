//
// Created by Tomas on 24. 11. 2020.
//

#ifndef CV9_BOOKSTATUS_H
#define CV9_BOOKSTATUS_H

enum class BookStatus {
    LOST, AVAILABLE, BORROWED
};

#endif //CV9_BOOKSTATUS_H
