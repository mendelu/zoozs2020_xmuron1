//
// Created by Tomas on 24. 11. 2020.
//

#ifndef CV9_LIBRARY_H
#define CV9_LIBRARY_H

#include "iostream"
#include <vector>
#include "Book.h"

class Library {
    static std::vector<Book *> s_books;

    Library() {};

public:

    static Book *createBook(std::string author, std::string title, int year);

    static void removeBookById(int bookId);

    static void printStatistics();

private:
    static std::vector<Book *> getBooksByStatus(BookStatus status);
};


#endif //CV9_LIBRARY_H
