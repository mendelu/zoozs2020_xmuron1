#include <iostream>

#include "Library.h"

int main() {
//    Library* library = new Library(); takto ne

    Library::createBook("James Patterson", "Zobani", 2004);
    Library::createBook("James Patterson", "Chozeni", 2005);
    Library::createBook("Andrew Child", "Stekani", 2012);

    Library::printStatistics();

    Library::removeBookById(1);

    Library::printStatistics();

    return 0;
}


