cmake_minimum_required(VERSION 3.15)
project(doucovani_2_12)

set(CMAKE_CXX_STANDARD 14)

add_executable(doucovani_2_12 main.cpp Zbozi.cpp Zbozi.h Potravina.cpp Potravina.h NakupniKosik.cpp NakupniKosik.h)