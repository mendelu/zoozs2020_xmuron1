//
// Created by Mikulas Muron on 02/12/2020.
//

#include "NakupniKosik.h"


void NakupniKosik::pridejZbozi(Zbozi *zbozi) {
    m_zbozi.push_back(zbozi);
}

void NakupniKosik::vypis() {
    for(Zbozi* z: m_zbozi){
        z->printInfo();
    }
}

void NakupniKosik::odeberPosledni() {
    m_zbozi.pop_back();
}