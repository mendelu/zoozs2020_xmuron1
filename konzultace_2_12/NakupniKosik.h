//
// Created by Mikulas Muron on 02/12/2020.
//

#ifndef DOUCOVANI_2_12_NAKUPNIKOSIK_H
#define DOUCOVANI_2_12_NAKUPNIKOSIK_H

#include <vector>
#include <iostream>
#include "Zbozi.h"

class NakupniKosik {
std::vector<Zbozi*> m_zbozi;

public:
    void pridejZbozi(Zbozi* zbozi);
    void vypis();
    void odeberPosledni();
};


#endif //DOUCOVANI_2_12_NAKUPNIKOSIK_H
