//
// Created by Mikulas Muron on 02/12/2020.
//

#include "Potravina.h"


Potravina::Potravina(std::string jmeno, float cena, std::string trvanlivost): Zbozi(jmeno, cena){
    m_trvanlivost = trvanlivost;
}

float Potravina::getCena() {
    return m_cena * 1.15;
}

void Potravina::printInfo() {
    std::cout << "Potravina " << m_jmeno << " s cenou " << getCena() << std::endl;
}