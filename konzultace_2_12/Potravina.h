//
// Created by Mikulas Muron on 02/12/2020.
//

#ifndef DOUCOVANI_2_12_POTRAVINA_H
#define DOUCOVANI_2_12_POTRAVINA_H

#include "Zbozi.h"

class Potravina: public Zbozi {
    std::string m_trvanlivost;
public:
    Potravina(std::string jmeno, float cena, std::string trvanlivost);
    void printInfo();
    float getCena();
};


#endif //DOUCOVANI_2_12_POTRAVINA_H
