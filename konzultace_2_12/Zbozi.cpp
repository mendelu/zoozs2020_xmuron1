//
// Created by Mikulas Muron on 02/12/2020.
//

#include "Zbozi.h"

Zbozi::Zbozi(std::string jmeno, float cena) {
    m_jmeno = jmeno;
    m_cena = cena;
}

float Zbozi::getCena() {
    return m_cena * 1.22;
}

void Zbozi::printInfo() {
    std::cout << "Obycejne zbozi " << m_jmeno << " s cenou " << getCena() << std::endl;
}