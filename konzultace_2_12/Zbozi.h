//
// Created by Mikulas Muron on 02/12/2020.
//

#ifndef DOUCOVANI_2_12_ZBOZI_H
#define DOUCOVANI_2_12_ZBOZI_H

#include <iostream>

class Zbozi {
protected:
    std::string m_jmeno;
    float m_cena;
public:
    Zbozi(std::string jmeno, float cena);
    virtual void printInfo();
    virtual float getCena();
};


#endif //DOUCOVANI_2_12_ZBOZI_H
