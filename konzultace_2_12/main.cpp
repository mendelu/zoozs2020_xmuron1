#include <iostream>
#include "Potravina.h"
#include "NakupniKosik.h"

int main() {
    Potravina* p = new Potravina("Chleba", 100, "10-10-2020");
    Zbozi* z = new Zbozi("Chleba", 100);
    NakupniKosik* kosik = new NakupniKosik();
    kosik->pridejZbozi(z);
    kosik->pridejZbozi(p);
    kosik->odeberPosledni();
    kosik->vypis();
    return 0;
}