//
// Created by Mikulas Muron on 09/12/2020.
//

#include "Prisera.h"


Prisera::Prisera() {
    m_stav = new Zaba();
}

void Prisera::privitej() {
    m_stav->privitej();
}

void Prisera::rozlucSe() {
    m_stav->rozlucSe();
}

void Prisera::polibek() {
    delete m_stav;
    m_stav = new Princ();
}

void Prisera::zacaruj() {
    delete m_stav;
    m_stav = new Zaba();
}