//
// Created by Mikulas Muron on 09/12/2020.
//

#ifndef DOUCKO_9_12_PRISERA_H
#define DOUCKO_9_12_PRISERA_H

#include "Zaba.h"
#include "Princ.h"

class Prisera {
private:
    Stav* m_stav;
public:
    Prisera();
    void privitej();
    void rozlucSe();
    void polibek();
    void zacaruj();
};


#endif //DOUCKO_9_12_PRISERA_H
