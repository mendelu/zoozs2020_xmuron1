//
// Created by Mikulas Muron on 09/12/2020.
//

#ifndef DOUCKO_9_12_STAV_H
#define DOUCKO_9_12_STAV_H


class Stav {

public:
    virtual void privitej() = 0;
    virtual void rozlucSe() = 0;
    // ~Stav();

};


#endif //DOUCKO_9_12_STAV_H
