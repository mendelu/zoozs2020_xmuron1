//
// Created by Mikulas Muron on 09/12/2020.
//

#ifndef DOUCKO_9_12_ZABA_H
#define DOUCKO_9_12_ZABA_H

#include "Stav.h"
#include <iostream>

class Zaba : public Stav {
public:
    void privitej();
    void rozlucSe();
};


#endif //DOUCKO_9_12_ZABA_H
